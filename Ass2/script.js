//country list selection
let countryData = ["Afghanistan", "Albania", "Algeria", "American Samoa", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British Indian Ocean Territory", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo (Brazzaville)", "Congo (Kinshasa)", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Johnston Atoll", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia", "Midway Islands", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "North Korea", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Helena", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the Islands", "South Korea", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands", "Wake Island", "Wallis and Futuna", "West Bank", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"];


var select = document.getElementById("pickcountries")
for (var i = 0; i < countryData.length; i++) {
    var opt = countryData[i];
    var e = document.createElement("option");
    e.textContent = opt;
    e.value = opt
    e.id = opt
    select.appendChild(e)
}
//function of confirm date and country
//此func负责导出地图，date的info最后save时存进class, 然后class再导入local storage
function CDconfirm() {
    var a = document.getElementById("pickcountries")
    var conName = a.value;
    getData("https://eng1003.monash/OpenFlights/airports", "showAirports", conName, "?country=")
    getData("https://eng1003.monash/OpenFlights/allroutes/", "CallB", conName, "?country=")
    var a1 = 0
    var a2 = 0
    // initMap(a2, a1)

}

var airports = []
var map = null
var data = []
var dairportsid = []//demostic airports
var layerid = []
var directRoutes = []
var selectedRoutes = []
var routeObject = []
var stops= []
var distance=0

//direct map to selected country !!!n use any more!!!
function mapPan() {
    var a1 = airports[0].latitude
    var a2 = airports[0].longitude
    map.panTo([a2, a1]);
    //store airports id in this country 把选中国家内机场存入variable
    for (i = 0; i < airports.length; i++) {
    var DA = airports[i].airportId;
    dairportsid.push(DA)
    }
}

//Origional airport confirm, get routes, draw line 获取选中机场航班信息，
function oAconfirm() {
    var oairName = document.getElementById("start-airport").value
    stops.push(oairName)
    var oAir = airports.filter(airports => { return airports.name === oairName })
    var oAirId = oAir[0].airportId//selected Airport ID
    var oAirLong = oAir[0].longitude//selected Airport longtitude
    var oAirLa = oAir[0].latitude////selected Airport latitude
    map.flyTo({ center: [oAirLong, oAirLa], zoom: 6 });
    for (var I = 0; I < data.length; I++) {
        var routeSouId = data[I].sourceAirportId
        var routeDesId = data[I].destinationAirportId
        if (routeSouId == oAirId) {
            var selectedRouteDesId = routeDesId
            var dAir = airports.filter(airports => { return airports.airportId == selectedRouteDesId })
            if (typeof dAir[0] == 'undefined') { continue }
            else {
                directRoutes.push(data[I])
                var dAirLong = dAir[0].longitude
                var dAirLa = dAir[0].latitude
                showRoute([oAirLong, oAirLa], [dAirLong, dAirLa], I.toString(), "red")
                layerid.push(I.toString())
            }
        }
    }
}


var routeId = []

// filter inter routes Find airpoutes of source airport and draw 
function aRconfirm() {
    for (var i = 0; i < layerid.length; i++) {
        var a = layerid[i]
        map.removeLayer(a)
        map.removeSource(a)
    }
    layerid = []
    var oAirName = document.getElementById("start-airport").value
    var eAirName = document.getElementById("end-airport").value
    var oAir = getObject(airports, oAirName)
    var eAir = getObject(airports, eAirName)
    var oAirLong = oAir.longitude
    var oAirLa = oAir.latitude
    var eAirLong = eAir.longitude
    var eAirLa = eAir.latitude
    for (i = 0; i < directRoutes.length; i++) {
        if (directRoutes[i].destinationAirportId == eAir.airportId) {
            showRoute([oAirLong, oAirLa], [eAirLong, eAirLa], countryData[i], "blue")
            routeId.push(countryData[i])
            map.flyTo({ center: [eAirLong, eAirLa], zoom: 6 });
            var select = document.getElementById("airline")
            var opt = directRoutes[i].airline + directRoutes[i].airlineId;
            var e = document.createElement("option");
            e.textContent = opt;
            e.value = opt
            e.id = opt
            select.appendChild(e)
        }
    }

}
function aIconfirm() {
    var oAirName = document.getElementById("start-airport").value
    var eAirName = document.getElementById("end-airport").value
    var routeId = document.getElementById("airline").value
    var oAir = getObject(airports, oAirName)
    var eAir = getObject(airports, eAirName)
    var bDate=document.getElementById("Bdate").value
    var eDate=document.getElementById("Edate").value
    routeObject.push({
        originAirport: oAir,
        destinationAirport: eAir,
        airLine: routeId,
        begin_Date: bDate,
        end_Date: eDate,
        distance: distance
    })
    var oSelection = document.getElementById("start-airport").value = eAirName//swap as origin
    var select = document.getElementById("airline")
    select.innerHTML = ""
    for (i = 0; i < select.options.length; i++) {
        select.options[i] = null
    }
}
// undo
function Undo() {
    console.log(routeObject)
    removeAllLayer()
    if (routeObject.length > 0) {
        routeObject.remove(routeObject.length - 1)
        console.log(routeObject)
        if (routeObject.length > 0) {
            document.getElementById("start-airport").value = routeObject[routeObject.length - 1].destinationAirport.name
            document.getElementById("end-airport").value = null
            document.getElementById("airline").value = null
            routeObject.map(function (item, index) {
                routeId.push("u" + index)
                showRoute([item.originAirport.longitude, item.originAirport.latitude], [item.destinationAirport.longitude, item.destinationAirport.latitude], "u" + index, "blue")
            })
        }
    }
}

function removeAllLayer() {
    for (var i = 0; i < routeId.length; i++) {
        var a = routeId[i]
        map.removeLayer(a)
        map.removeSource(a)
    }
    routeId = []
}

function save() {
    var eAirName = document.getElementById("end-airport").value
    stops.push(eAirName)
    routeObject.originAirport=stops[0]
    routeObject.destinationAirport=stops[stops.length - 1]
    if (localStorage.getItem("routeList"))
        localStorage.removeItem("routeList")
    if (routeObject.length > 0) {
        let list = new TripList()
        routeObject.map(function (item, index) {
            let tripObj = new trip(index)
            tripObj.Oairport = item.originAirport.name
            tripObj.Dairport = item.destinationAirport.name
            tripObj.stops = stops
            tripObj.numberOfstops =stops.length
            tripObj.distance = distance+"km"
            tripObj.beginDate = item.begin_Date
            tripObj.endDate = item.end_Date
            console.log(tripObj)
            list.addTrip(tripObj.getId)
            console.log(list)
        })

        localStorage.setItem("routeList", JSON.stringify(list))
    }
}
//map initiation

function initMap(c1, c2) {
    mapboxgl.accessToken = 'pk.eyJ1IjoiYWxlY2NhaSIsImEiOiJja2xscWM0bmkwMXRmMnBxajVwcWVxNW8xIn0.4DSIVs1qG6P74Y7bm1tlsA';
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/light-v9',
        center: [c1, c2],
        zoom: 3.7
    });
}

function addPoint(data) {
    data.map(function (item, index) {
        var marker = new mapboxgl.Marker().setLngLat([item.longitude, item.latitude])//添加marker的初始化点
            .addTo(map);//在哪个地图中添加

    })
}

// api
function getData(url, callback, item, SpecialQ) {
    let QueryString = url + SpecialQ + item + "&callback=" + callback
    let script = document.createElement('script');
    script.src = QueryString
    document.body.appendChild(script);

}


function CallB(Data) {
    console.log(Data)
    data = Data
}

function showAirports(results) {
    airports = results
    console.log(airports)
    var a1 = 0
    var a2 = 0
    if (airports.length > 0) {
        a1 = airports[0].latitude
        a2 = airports[0].longitude
    }

    initMap(a2, a1)
    addPoint(results)
    addOptions("start-airport", results)
    addOptions("end-airport", results)
}

// 动态添加option
function addOptions(id, data) {
    data.map(function (item) {
        var div = document.createElement('option');
        div.innerHTML = item.name;
        div.style.color = 'red';
        div.setAttribute("lon", item.longitude);
        div.setAttribute("lat", item.latitude);
        document.getElementById(id).appendChild(div)
    })
}

// object func
function getObject(data, name) {
    var obj = data.filter(function (item) {
        return item.name == name
    })
    if (obj.length > 0)
        return obj[0]
    return {}
}

function searchRoutes() {
    var start = document.getElementById("start-airport").value
    var end = document.getElementById("end-airport").value
    var startPoint = getObject(airports, start)
    var endPoint = getObject(airports, end)
    console.log(startPoint, endPoint)
    showRoute([startPoint.longitude, startPoint.latitude], [endPoint.longitude, endPoint.latitude])
}
// draw
function showRoute(s, e, id, color) {
    //mao.removeLayer
    var route = {
        "id": "route",
        'type': 'FeatureCollection',
        'features': [
            {
                'type': 'Feature',
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [s, e]
                }
            }
        ]
    };

    // distance
    var lineDistance = turf.lineDistance(route.features[0], 'kilometers');
    var stepArr = [];
    var steps = 2000;
    for (var i = 1; i < lineDistance; i += lineDistance / steps) {
        var lnglat = turf.along(route.features[0], i, 'kilometers');
        stepArr.push(lnglat.geometry.coordinates);
    }
    distance+=lineDistance
    map.addLayer({
        "id": id,
        "type": "line",
        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": stepArr
                }
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round",
        },
        "paint": {
            "line-color": color,
            "line-width": 2,
        }
    });
}

/**
*删除数组指定下标或指定对象
*/
Array.prototype.remove = function (obj) {
    for (var i = 0; i < this.length; i++) {
        var temp = this[i];
        if (!isNaN(obj)) {
            temp = i;
        }
        if (temp == obj) {
            for (var j = i; j < this.length; j++) {
                this[j] = this[j + 1];
            }
            this.length = this.length - 1;
        }
    }
}